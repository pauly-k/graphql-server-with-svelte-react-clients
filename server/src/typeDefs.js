const typeDefs = `
	type Todo {
		id: ID!
		title: String!
	}

	type Query {
		appTitle: String!
		todos(offset: Int, limit: Int): [Todo!]!
	}
`

module.exports = { typeDefs }