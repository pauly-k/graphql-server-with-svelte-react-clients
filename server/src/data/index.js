const fetch = require('node-fetch')

async function getTodos(offset = 0, limit = Number.MAX_SAFE_INTEGER) {
	const url = 'https://jsonplaceholder.typicode.com/todos'
	const todos = await  fetch(url).then(resp => resp.json())
	return todos.slice(offset, offset + limit)
}

module.exports = { getTodos }