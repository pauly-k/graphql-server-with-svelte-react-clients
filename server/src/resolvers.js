const { getTodos } = require('./data')

const resolvers = {
	Query: {
		appTitle: () => "Graphql Client Dude!",
		todos: async (_, {offset, limit}) => getTodos(offset, limit)
	}
}

module.exports = { resolvers }