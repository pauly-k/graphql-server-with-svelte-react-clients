# Demo Apollo Graphql server + react and svelte clients

A simple graphql server with react and svelte clients consuming the graphql endpoint.

Using `fetch` and http post requests to interact with the graphql server.

```javascript
export async function loadAppTitle () {
  const resp = await fetch(graphqlEndpoint, {
    method: 'POST',
    headers: { 'content-type': 'application/json'},
    body: JSON.stringify({ query: `{ appTitle }`})
  }).then(resp => resp.json())

  return resp.data.appTitle
}
```
