import React from 'react'


function TodoList ({ todos }) {
	
let todoItems = todos.map((todo, index) => <div className="item">{todo.title}</div>)

return <div className="list">{todoItems}</div>
}

export default TodoList