const graphqlEndpoint = 'http://localhost:4000/graphql'

export async function loadAppTitle () {
  const resp = await fetch(graphqlEndpoint, {
    method: 'POST',
    headers: { 'content-type': 'application/json'},
    body: JSON.stringify({ query: `{ appTitle }`})
  }).then(resp => resp.json())

  return resp.data.appTitle
}

export async function loadTodos() {
	const resp = await fetch(graphqlEndpoint, {
		method: 'POST',
		headers: { 'content-type': 'application/json'},
		body: JSON.stringify({ query: `{
			todos {
				title
			}
		}`})
	}).then(resp => resp.json())

	console.log(resp.data);

	return resp.data.todos
}