import React, { useState, useEffect } from 'react';
import TodoList from './components/TodoList'
import './App.css';
import { loadAppTitle, loadTodos } from './data';




function App() {

  const [appTitle, setAppTitle] = useState('');
  const [todos, setTodos] = useState([])

  
  useEffect(() => {
    async function loadAndSetAppTitle() {
      let appTitle = await loadAppTitle()
      setAppTitle(appTitle)
    }

    loadAndSetAppTitle()
  }, [appTitle])

  useEffect(() => {

    async function loadAndSettodos() {
      let todos = await loadTodos()
      setTodos(todos)
    }

    loadAndSettodos()
  }, [todos])

  return (
    <div className="App">
      <header className="App-header">
      ` <h2>{appTitle}</h2>
        <p>
          Edit <code>src/App.js</code> and save to reload.
        </p>
        <a
          className="App-link"
          href="https://reactjs.org"
          target="_blank"
          rel="noopener noreferrer"
        >
          Learn React
        </a>
      </header>

      <TodoList todos={todos}  />


    </div>
  );
}

export default App;
